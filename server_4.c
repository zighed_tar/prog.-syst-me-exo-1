#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/wait.h>


bool running = true; 
char * forkid = "père";

void stop_handler(int signum) {
  printf("%s : Le signal %d.\n", forkid, signum);
  running = 0;
}
void exit_message(void) {
  printf("%s : Fin du programme\n",forkid);
}


int main(int argc, char * argv[]) {
  //pipe : 
  int pipe_ends[2];
  if (pipe(pipe_ends) < 0) {
    exit(EXIT_FAILURE);
  }

  //
  atexit(&exit_message);
  printf("%s :Début du programme\n",forkid);

  struct sigaction action;
  action.sa_handler = stop_handler;
  action.sa_flags = 0;
  sigaction (SIGINT, &action, NULL);
  sigaction(SIGTERM, &action, NULL);

int pid = fork(); // on fork lepid 

  // En cas d'erreur de création 
  if (pid < 0) {
    fprintf(stderr, "%s: Erreur de création", forkid);
  }
  // Si 0 alors fils 
  if (pid == 0) {  
    forkid = "fils";
    close(pipe_ends[1]);

  while(running) {
    //déclaration de variables : 
  int id = getpid();
  int idpere = getppid();
  int nombre;
  // code :
  printf("le id : %d\n", id);
  printf("le id père : %d\n", idpere);
  if (read(pipe_ends[0], &nombre, sizeof(int)) > 0) {
        printf("%s: nombre lu %d\n", forkid, nombre);
      } else {
        printf("%s: fin.\n", forkid);
        running = 0;
      }
  }
  close(pipe_ends[0]);
  } else {
    close(pipe_ends[0]);
    forkid = "père";
    while(running) {
    //déclaration de variables : 
      int id = getpid();
      int idpere = getppid();
      int randNum;
      // code :
      printf("le id : %d\n", id);
      printf("%s : le id père : %d\n",forkid, idpere);
      randNum=rand() % 100;
      printf("random %d\n", randNum);
      write(pipe_ends[1], &randNum, sizeof(int));
      sleep(1);
      }
      close(pipe_ends[1]);
  }
  // le père
 if (pid != 0) { 
    int status;
    printf("%s: attendre.\n", forkid);
    wait(&status);
    printf("%s: status du fils = %d.\n", forkid, status);
  }

  return EXIT_SUCCESS;
}