#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>

bool running = true; 

void stop_handler(int signum) {
  printf("Le signal %d.\n", signum);
  running = 0;
}
void exit_message() {
  printf("Fin du programme\n");
}


int main(int argc, char * argv[]) {
  atexit(&exit_message);
  printf("Début du programme\n");

  struct sigaction action;
  action.sa_handler = stop_handler;
  action.sa_flags = 0;
  sigaction (SIGINT, &action, NULL);
  sigaction(SIGTERM, &action, NULL);

  while(running) {
    //déclaration de variables : 
  int id = getpid();
  int idpere = getppid();
  int randNum;

  // code :
  printf("le id : %d\n", id);
  printf("le id père : %d\n", idpere);
  randNum=rand() % 100;
  printf("random %d\n", randNum);
  sleep(1);
  }
 

  return EXIT_SUCCESS;
}