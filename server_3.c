#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/wait.h>


bool running = true; 
char * forkid = "père";

void stop_handler(int signum) {
  printf("%s : Le signal %d.\n", forkid, signum);
  running = 0;
}
void exit_message(void) {
  printf("%s : Fin du programme\n",forkid);
}


int main(int argc, char * argv[]) {
  atexit(&exit_message);
  printf("%s :Début du programme\n",forkid);

  struct sigaction action;
  action.sa_handler = stop_handler;
  action.sa_flags = 0;
  sigaction (SIGINT, &action, NULL);
  sigaction(SIGTERM, &action, NULL);

int pid = fork(); // on fork lepid 

  // En cas d'erreur de création 
  if (pid < 0) {
    fprintf(stderr, "%s: Erreur de création", forkid);
  }
  // Si 0 alors fils 
  if (pid == 0) {  
    forkid = "fils";
  } else { // sinon c'est  le père
    forkid = "père";
  }
  while(running) {
    //déclaration de variables : 
  int id = getpid();
  int idpere = getppid();
  int randNum;

  // code :
  printf("le id : %d\n", id);
  printf("le id père : %d\n", idpere);
  randNum=rand() % 100;
  printf("random %d\n", randNum);
  sleep(1);
  }
  // le père
 if (pid != 0) { 
    int status;
    printf("%s: attendre.\n", forkid);
    wait(&status);
    printf("%s: status du fils %d.\n", forkid, status);
  }

  return EXIT_SUCCESS;
}